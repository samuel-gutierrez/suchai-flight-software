#include "cmdSTT.h"

static const char *tag = "cmdSTT";

void cmd_stt_init(void)
{
    cmd_add("stt_test", stt_test, "%d", 1);
    cmd_add("stt_reboot", stt_reboot, "%d", 1);
    cmd_add("stt_test_mux", stt_test_mux, "%d %s", 2);
    cmd_add("stt_take_one_pic", stt_take_one_pic, "%d %s", 2);
    cmd_add("stt_take_pic_no_exp", stt_take_pic_no_exp, "%d", 1);
    cmd_add("stt_evaluate_pic", stt_evaluate_pic, "%d %s", 2);
    cmd_add("stt_angles_from_gyro", stt_angles_from_gyro, "%d", 1);
    cmd_add("stt_solve_lis_one_pic", stt_solve_lis_one_pic, "%d %s", 2);
    cmd_add("stt_solve_lis_and_track", stt_solve_lis_and_track, "%d %s %s", 3);
    cmd_add("stt_take_pic_flir", stt_take_pic_flir, "%d", 1);
    cmd_add("stt_led_on", stt_led_on, "%d %s", 2);
    cmd_add("stt_chamber_pic", stt_chamber_pic, "%d", 1);
    cmd_add("stt_rpi_temp", stt_rpi_temp, "%d", 1);
}

int stt_test(char* fmt, char* params, int nparams)
{
    int node;

    if(params == NULL || sscanf(params, fmt, &node) != nparams)
        return CMD_ERROR;

    printf("--> STT: OK!\n");
    system("pwd");
    char *buff = "OK\n";
    /*
    int ok = 1;
    int *data = {1, 2, 3};
    */

    int rc;
    rc = _com_send_data(node, buff, 4, 30, 1);
    /*
    rc = _com_send_data(node, &ok, sizeof(ok), 30, 1);
    rc = _com_send_data(node, data, sizeof(int), 30, 4);
    */
    return CMD_OK;
}

int stt_reboot(char* fmt, char* params, int nparams)
{
    int node;

    if(params == NULL || sscanf(params, fmt, &node) != nparams)
        return CMD_ERROR;

    printf("--> STT: Reboot... \n");
    system("sudo reboot");
    /*
    int rc = _com_send_data(node, (void *) CMD_OK, sizeof(CMD_OK), 11, 1);
    */
    return CMD_OK;
}

int stt_test_mux(char* fmt, char* params, int nparams)
{
    int node;
    char cam_nmbr[SCH_CMD_MAX_STR_PARAMS];
    char task[SCH_CMD_MAX_STR_PARAMS];

    if(params == NULL || sscanf(params, fmt, &node, cam_nmbr) != nparams)
        return CMD_ERROR;

    snprintf(task, sizeof(task), "python3 /home/pi/suchai-flight-software/stt_python/test_mux.py %s", cam_nmbr);

    system(task);

    /*
    int rc = _com_send_data(node, (void *) CMD_OK, sizeof(CMD_OK), 11, 1);
    */
    return CMD_OK;
}

int stt_take_one_pic(char* fmt, char* params, int nparams)
{
    int node;
    char exp_time[SCH_CMD_MAX_STR_PARAMS];
    char task[SCH_CMD_MAX_STR_PARAMS];

    if(params == NULL || sscanf(params, fmt, &node, exp_time) != nparams)
        return CMD_ERROR;

    snprintf(task, sizeof(task), "python3 /home/pi/suchai-flight-software/stt_python/take_one_picture.py %s", exp_time);

    system(task);
    /*
    int rc = _com_send_data(node, (void *) CMD_OK, sizeof(CMD_OK), 11, 1);
    */
    return CMD_OK;
}

int stt_take_pic_no_exp(char* fmt, char* params, int nparams)
{
    int node;

    if(params == NULL || sscanf(params, fmt, &node) != nparams)
        return CMD_ERROR;

    system("python3 /home/pi/suchai-flight-software/stt_python/take_one_pic_no_exposure.py");

    /*
    int rc = _com_send_data(node, (void *) CMD_OK, sizeof(CMD_OK), 11, 1);
    */
    return CMD_OK;
}

int stt_evaluate_pic(char* fmt, char* params, int nparams)
{
    int node;
    char exp_time[SCH_CMD_MAX_STR_PARAMS];
    char task[SCH_CMD_MAX_STR_PARAMS];

    if(params == NULL || sscanf(params, fmt, &node, exp_time) != nparams)
        return CMD_ERROR;

    snprintf(task, sizeof(task), "python3 /home/pi/suchai-flight-software/stt_python/take_one_pic_and_eval.py %s", exp_time);

    system(task);
    /*
    int rc = _com_send_data(node, (void *) CMD_OK, sizeof(CMD_OK), 11, 1);
    */
    return CMD_OK;
}

int stt_angles_from_gyro(char* fmt, char* params, int nparams)
{
    int node;

    if(params == NULL || sscanf(params, fmt, &node) != nparams)
        return CMD_ERROR;

    system("python3 /home/pi/suchai-flight-software/stt_python/angles_from_gyro.py");

    /*
    int rc = _com_send_data(node, (void *) CMD_OK, sizeof(CMD_OK), 11, 1);
    */
    return CMD_OK;
}

int stt_solve_lis_one_pic(char* fmt, char* params, int nparams)
{
    if(params == NULL)
    {
        LOGE(tag, "Null arguments!");
        return CMD_ERROR;
    }

    int node;
    char exp_time[SCH_CMD_MAX_STR_PARAMS];

    char task[SCH_CMD_MAX_STR_PARAMS];
    char filename[SCH_CMD_MAX_STR_PARAMS];
    memset(filename, '\0', SCH_CMD_MAX_STR_PARAMS);

    if(sscanf(params, fmt, &node, exp_time) == nparams) {
        char *filename = "/home/pi/suchai-flight-software/stt_data/lis_one_image.txt"; // Path to file with data of the angle

        snprintf(task, sizeof(task), "python3 /home/pi/suchai-flight-software/stt_python/solve_lis_one_image.py %s", exp_time);
        system(task); //Call script

        FILE *myfile;

        // Create structure, read file and save values
        stt_angle_t angle;
        memset(&angle, 0, sizeof(angle));
        myfile = fopen(filename, "r");

        if (fscanf(myfile, "%f %f %f", &angle.ra, &angle.dec, &angle.roll) == 3) {
            fclose(myfile);
            printf("%f %f %f", angle.ra, angle.dec, angle.roll); //For debugging purposes

            // Send angle data to node
            int rc = _com_send_data(node, (void *) &angle, sizeof(angle), 11, 1);

            return CMD_OK;
        }
        fclose(myfile);
        LOGE(tag, "Error parsing data")
        return CMD_FAIL;
    }

    LOGE(tag, "Error parsing parameters!");
    return CMD_FAIL;
}

int stt_solve_lis_and_track(char* fmt, char* params, int nparams)
{
    int node;
    char exp_time[SCH_CMD_MAX_STR_PARAMS];
    char n_track[SCH_CMD_MAX_STR_PARAMS];
    char task[SCH_CMD_MAX_STR_PARAMS];

    if(params == NULL || sscanf(params, fmt, &node, exp_time, n_track) != nparams)
        return CMD_ERROR;

    snprintf(task, sizeof(task), "python3 /home/pi/suchai-flight-software/stt_python/solve_lis_and_tracking.py %s %s", exp_time, n_track);

    system(task);
    /*
    int rc = _com_send_data(node, (void *) CMD_OK, sizeof(CMD_OK), 11, 1);
    */
    return CMD_OK;
}

int stt_take_pic_flir(char* fmt, char* params, int nparams)
{
    int node;

    if(params == NULL || sscanf(params, fmt, &node) != nparams)
        return CMD_ERROR;

    system("python3 /home/pi/suchai-flight-software/stt_python/flir_picture.py");

    /*
    int rc = _com_send_data(node, (void *) CMD_OK, sizeof(CMD_OK), 11, 1);
    */
    return CMD_OK;
}

int stt_led_on(char* fmt, char* params, int nparams)
{
    int node;
    char n_led[SCH_CMD_MAX_STR_PARAMS];
    char task[SCH_CMD_MAX_STR_PARAMS];

    if(params == NULL || sscanf(params, fmt, &node, n_led) != nparams)
        return CMD_ERROR;

    snprintf(task, sizeof(task), "python3 /home/pi/suchai-flight-software/stt_python/turn_on_led.py %s",n_led);

    system(task);
    /*
    int rc = _com_send_data(node, (void *) CMD_OK, sizeof(CMD_OK), 11, 1);
    */
    return CMD_OK;
}

int stt_chamber_pic(char* fmt, char* params, int nparams)
{
    int node;

    if(params == NULL || sscanf(params, fmt, &node) != nparams)
        return CMD_ERROR;

    system("python3 /home/pi/suchai-flight-software/stt_python/take_chamber_pic.py");

    /*
    int rc = _com_send_data(node, (void *) CMD_OK, sizeof(CMD_OK), 11, 1);
    */
    return CMD_OK;
}

int stt_rpi_temp(char* fmt, char* params, int nparams)
{
    int node;

    char *cmd = "vcgencmd measure_temp | egrep -o '[0-9]*\.[0-9]*'";
    char buf[SCH_CMD_MAX_STR_PARAMS];
    FILE *fp;

    if(params == NULL || sscanf(params, fmt, &node) != nparams)
        return CMD_ERROR;

    if ((fp = popen(cmd, "r")) == NULL) {
        printf("Error opening pipe!\n");
        return CMD_ERROR;
    }

    while (fgets(buf, SCH_CMD_MAX_STR_PARAMS, fp) != NULL) {
        printf("OUTPUT: %s", buf);
    }

    if(pclose(fp))  {
        printf("Command not found or exited with error status\n");
        return CMD_ERROR;
    }

    /*
    int rc = _com_send_data(node, (void *) CMD_OK, sizeof(CMD_OK), 11, 1);
    */
    return CMD_OK;
}
