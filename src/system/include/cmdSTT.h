#include "globals.h"
#include "config.h"
#include "repoCommand.h"

typedef struct __attribute__((__packed__)) stt_angle{
    float ra;
    float dec;
    float roll;
}stt_angle_t;

void cmd_stt_init(void);
int stt_test(char* fmt, char* params, int nparams);
int stt_reboot(char* fmt, char* params, int nparams);
int stt_test_mux(char* fmt, char* params, int nparams);
int stt_take_one_pic(char* fmt, char* params, int nparams);
int stt_take_pic_no_exp(char* fmt, char* params, int nparams);
int stt_evaluate_pic(char* fmt, char* params, int nparams);
int stt_angles_from_gyro(char* fmt, char* params, int nparams);
int stt_solve_lis_one_pic(char* fmt, char* params, int nparams);
int stt_solve_lis_and_track(char* fmt, char* params, int nparams);
int stt_take_pic_flir(char* fmt, char* params, int nparams);
int stt_led_on(char* fmt, char* params, int nparams);
int stt_chamber_pic(char* fmt, char* params, int nparams);
int stt_rpi_temp(char* fmt, char* params, int nparams);
