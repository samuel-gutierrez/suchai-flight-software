import RPi.GPIO as gp
import sys
import time


gp.setwarnings(False)
gp.setmode(gp.BOARD)
led1, led2, led3, led4 = (33, 37, 40, 38)
gp.setup(led1, gp.OUT)
gp.setup(led2, gp.OUT)
gp.setup(led3, gp.OUT)
gp.setup(led4, gp.OUT)

led_nmbr = int(sys.argv[1])

if led_nmbr == 1:
    print("--> STT: Turning on LED 1 for 5 seconds")
    gp.output(led1, True)
    time.sleep(5)
    gp.output(led1, False)
    gp.clenaup()
elif led_nmbr == 2:
    print("--> STT: Turning on LED 2 for 5 seconds")
    gp.output(led2, True)
    time.sleep(5)
    gp.output(led2, False)
    gp.clenaup()
elif led_nmbr == 3:
    print("--> STT: Turning on LED 3 for 5 seconds")
    gp.output(led3, True)
    time.sleep(5)
    gp.output(led3, False)
    gp.clenaup()
elif led_nmbr == 4:
    print("--> STT: Turning on LED 4 for 5 seconds")
    gp.output(led4, True)
    time.sleep(5)
    gp.output(led4, False)
    gp.clenaup()
elif led_nmbr == 12:
    print("--> STT: Turning on LED 1 and LED 2 for 5 seconds")
    gp.output(led1, True)
    gp.output(led2, True)
    time.sleep(5)
    gp.output(led1, False)
    gp.output(led2, False)
    gp.clenaup()
elif led_nmbr == 123:
    print("--> STT: Turning on LED 1, LED 2 and LED 3 for 5 seconds")
    gp.output(led1, True)
    gp.output(led2, True)
    gp.output(led3, True)
    time.sleep(5)
    gp.output(led1, False)
    gp.output(led2, False)
    gp.output(led3, False)
    gp.clenaup()
elif led_nmbr == 1234:
    print("--> STT: Turning on LED 1, LED 2, LED 3 and LED 4 for 5 seconds")
    gp.output(led1, True)
    gp.output(led2, True)
    gp.output(led3, True)
    gp.output(led4, True)
    time.sleep(5)
    gp.output(led1, False)
    gp.output(led2, False)
    gp.output(led3, False)
    gp.output(led4, False)
    gp.clenaup()
else:
    raise ValueError('---> ERROR: Please select a valid led number!')
