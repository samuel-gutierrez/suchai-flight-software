import os
import RPi.GPIO as gp
import subprocess
import time

gp.setwarnings(True)
gp.setmode(gp.BCM)

ep, f1p = [17, 4]

gp.setup(ep, gp.OUT)
gp.setup(f1p, gp.OUT)

print("--> STT: Taking picture without setting exposure time")

os.system("i2cset -y 1 0x70 0x00 0x01")
gp.output(ep, False)
gp.output(f1p, False)

current_time = time.strftime("%d%m%y_%H%M%S", time.localtime())
pic_name = "/home/pi/STT_pictures/{}_noexp.jpg".format(current_time)
task = 'raspistill -w 1024 -h 1024 -t 1 -o {}'.format(pic_name)
print('EXECUTING: ', task)
process = subprocess.Popen(task, shell=True, stdout=subprocess.PIPE)
process.wait()
gp.cleanup()
