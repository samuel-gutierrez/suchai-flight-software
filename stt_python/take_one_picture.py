import os
import RPi.GPIO as gp
import subprocess
import sys
import time

gp.setwarnings(True)
gp.setmode(gp.BCM)

ep, f1p = [17, 4]

gp.setup(ep, gp.OUT)
gp.setup(f1p, gp.OUT)

exp_time = sys.argv[1]
print("--> STT: Taking picture with {} ms of exposure time".format(exp_time))

os.system("i2cset -y 1 0x70 0x00 0x01")
gp.output(ep, False)
gp.output(f1p, False)

time_ms = int(exp_time)
time_micros = 1000 * time_ms
current_time = time.strftime("%d%m%y_%H%M%S", time.localtime())
pic_name = "/home/pi/STT_pictures/{}_{}ms.jpg".format(current_time, time_ms)
task = 'raspistill -w 1024 -h 1024 -t 1 -ss {} -o {}'.format(time_micros, pic_name)
print('EXECUTING: ', task)
process = subprocess.Popen(task, shell=True, stdout=subprocess.PIPE)
process.wait()
gp.cleanup()
