import os
import RPi.GPIO as gp
import subprocess
import time

gp.setwarnings(True)
gp.setmode(gp.BOARD)
led1, led2, led3, led4 = (33, 37, 40, 38)
gp.setup(led1, gp.OUT)
gp.setup(led2, gp.OUT)
gp.setup(led3, gp.OUT)
gp.setup(led4, gp.OUT)
ep, f1p = (11, 7)
gp.setup(ep, gp.OUT)
gp.setup(f1p, gp.OUT)

print("--> STT: Taking picture of chamber")

os.system("i2cset -y 1 0x70 0x00 0x02")
gp.output(ep, False)
gp.output(f1p, True)

current_time = time.strftime("%d%m%y_%H%M%S", time.localtime())
pic_name = "/home/pi/chamber_pictures/{}.jpg".format(current_time)
task = 'raspistill -w 1024 -h 1024 -t 1 -o {}'.format(pic_name)
print('EXECUTING: ', task)

gp.output(led1, True)
gp.output(led2, True)
gp.output(led3, True)
gp.output(led4, True)
process = subprocess.Popen(task, shell=True, stdout=subprocess.PIPE)
process.wait()
gp.output(led1, False)
gp.output(led2, False)
gp.output(led3, False)
gp.output(led4, False)
gp.cleanup()
