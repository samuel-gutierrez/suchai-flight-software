import numpy as np
import os
import RPi.GPIO as gp
import subprocess
import sys
import time
from PIL import Image


gp.setwarnings(True)
gp.setmode(gp.BCM)

ep, f1p = [17, 4]

gp.setup(ep, gp.OUT)
gp.setup(f1p, gp.OUT)

exp_time = sys.argv[1]
print("--> STT: Taking picture with {} ms of exposure time and then analyzing it".format(exp_time))

os.system("i2cset -y 1 0x70 0x00 0x01")
gp.output(ep, False)
gp.output(f1p, False)

time_ms = int(exp_time)
time_micros = 1000 * time_ms
current_time = time.strftime("%d%m%y_%H%M%S", time.localtime())
pic_name = "/home/pi/STT_pictures/{}_{}ms.jpg".format(current_time, time_ms)
task = 'raspistill -w 1024 -h 1024 -t 1 -ss {} -o {}'.format(time_micros, pic_name)
print('EXECUTING: ', task)
process = subprocess.Popen(task, shell=True, stdout=subprocess.PIPE)
process.wait()
gp.cleanup()

# Validate image
pic_name = "/home/pi/suchai-flight-software/stt_data/sample_pic.jpg"
img = Image.open(pic_name)
img_bw = img.convert('L')

# Image to matrix 1024x1024
im1_array = np.array(img_bw)
columna, fila = img_bw.size

a_im1 = []
for i in range(columna):
    b = 0
    for j in range(fila):
        b = b + im1_array[j, i]
    a_im1.append(b)

peak = []
e_x = []
for k in range(len(a_im1)):
    if k == 0:
        if a_im1[k] > a_im1[k+1]:
            peak.append(a_im1[k])
            e_x.append(k)
    elif k > 0 and k < (len(a_im1)-1):
        if a_im1[k] > a_im1[k-1] and a_im1[k] > a_im1[k+1]:
            peak.append(a_im1[k])
            e_x.append(k)
    elif k == (len(a_im1) - 1):
        if a_im1[k] > a_im1[k-1]:
            peak.append(a_im1[k])
            e_x.append(k)

p = filter(lambda x: x > max(a_im1)*0.1, peak)
l_p = list(p)

print("peaks: ", len(l_p))
