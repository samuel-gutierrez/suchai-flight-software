import os
import RPi.GPIO as gp
import subprocess
import sys
import time

gp.setwarnings(True)
gp.setmode(gp.BCM)

ep, f1p = [17, 4]

gp.setup(ep, gp.OUT)
gp.setup(f1p, gp.OUT)

cam_nmbr = int(sys.argv[1])
print("--> STT: Taking picture with camera number {}".format(cam_nmbr))

try:
    if cam_nmbr == 1:
        os.system("i2cset -y 1 0x70 0x00 0x01")
        gp.output(ep, False)
        gp.output(f1p, False)
        cmd = "raspistill -n -t 1 -v -o /home/pi/suchai-flight-software/capture_1.jpg"
        process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        process.wait()
        time.sleep(1)
        if os.path.isfile("/home/pi/suchai-flight-software/capture_1.jpg"):
            os.remove("/home/pi/suchai-flight-software/capture_1.jpg")
        else:
            raise FileNotFoundError
        print('CAM 1 OK!')
    elif cam_nmbr == 2:
        os.system("i2cset -y 1 0x70 0x00 0x02")
        gp.output(ep, False)
        gp.output(f1p, True)
        cmd = "raspistill -n -t 1 -v -o /home/pi/suchai-flight-software/capture_2.jpg"
        process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        process.wait()
        time.sleep(1)
        if os.path.isfile("/home/pi/suchai-flight-software/capture_2.jpg"):
            os.remove("/home/pi/suchai-flight-software/capture_2.jpg")
        else:
            raise FileNotFoundError
        print('CAM 2 OK!')
    else:
        print('---> Doing nothing! Please select camera <1> or <2>')
    gp.cleanup()
except Exception as err:
    print(err)
    gp.cleanup()
